Ohai Plugins
===========

Usage
----

You can refer to the [Distributing Ohai Plugins][1]. But the short story is, 
check out the [ohai plugin][2] and drop the plugins into the 
ohai/files/default/plugins directory.

openstack
---------

The openstack plugin collects all the data stored in the metadata url, 
http://169.254.169.254/latest/. The data is stored in the nodes attributes in
the same format as the url method used to retrieve it. 

For example, http://169.254.169.254/latest/meta-data/public-ipv4 is stored in 
node.openstack.latest.meta-data.public-ipv4.

[1]: http://wiki.opscode.com/display/chef/Distributing+Ohai+Plugins
[2]: https://github.com/opscode-cookbooks/ohai
