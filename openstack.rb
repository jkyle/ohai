#
# Author:: James Kyle <james.kyle@att.com>
# License:: Apache License, Version 2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
require 'net/http'

provides "openstack"

openstack Mash.new

META_IP = "169.254.169.254"
BASE    = "/latest/"
HTTP    = Net::HTTP.new(META_IP)
HTTP.open_timeout = 1

def build_method_mash(mash, method, prev_response=nil)
  key = ::File.basename(method)
  resp = HTTP.request(Net::HTTP::Get.new(method))
  code = resp.code
  methods = resp.body.split

  if code == "200" and not methods.eql? prev_response then
    if key.eql? "public-keys" then
      mash[key] = parse_public_keys(methods)
    elsif key.eql? "security-groups" then
      mash[key] = methods
    elsif methods.length == 1 and not methods.eql? prev_response then
      mash[key] = resp.body
    else
      mash[key] = Mash.new
      methods.each do |meth|
        build_method_mash(mash[key], [method, meth].join("/"), methods)
      end
    end
  end
end

def parse_public_keys(keys)
  key_mash = Mash.new
  keys.each do |key|
    id, name = key.split("=")
    req = Net::HTTP::Get.new("#{BASE}/meta-data/public-keys/#{id}/openssh-key")
    resp = HTTP.request(req)
    key_mash[name] = resp.body
  end
  return key_mash 
end

begin
  build_method_mash(openstack, BASE)
rescue Timeout::Error
end


